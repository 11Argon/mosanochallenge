import React from "react";
import "../css/Style.css";
import Form from "../components/Form";
import { UserI, InitialStoreI } from "../objects";
import { useSelector, useDispatch } from "react-redux";
import {
  setOldEntries,
  setAlertAnimation,
  logOut,
  setSelectedUser,
} from "../actions";
import { withNamespaces } from "react-i18next";

const FormContainer = ({ routes, location, t }: any) => {
  const { pathname } = location;
  const dispatch = useDispatch();
  const users = useSelector((store: InitialStoreI) => store.data.oldEntries);
  const animate = useSelector((store: InitialStoreI) => store.data.animate);
  const countries = useSelector((store: InitialStoreI) => store.data.countries);
  const isLogged = useSelector((store: InitialStoreI) => store.auth.isLogged);
  const username = useSelector((store: InitialStoreI) => store.auth.username);
  const language = useSelector(
    (store: InitialStoreI) => store.application.language
  );
  const selectedUser = useSelector(
    (store: InitialStoreI) => store.data.selectedUser
  );

  const handleSaveUser = (user: UserI) => {
    let usersClone = [...users];
    user.id = usersClone.length;
    user.selected = true;
    usersClone.push({ ...user });
    dispatch(setSelectedUser(user));
    dispatch(setOldEntries(usersClone));
    dispatch(setAlertAnimation(true));
  };
  const handleLogout = () => {
    dispatch(logOut());
  };

  const handleAnimationEnd = (e: any) => {
    dispatch(setAlertAnimation(false));
  };

  const calculateSubmitMessage = (lastSubmit: UserI) => {
    const monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December",
    ];
    let { birthday = "" } = lastSubmit;
    let birthDate = new Date(birthday);
    let todaysDate = new Date();
    let age = todaysDate.getFullYear() - birthDate.getFullYear();
    let displayCountry =
      language === "pt"
        ? countries.find((c) => c.alpha2Code === lastSubmit.country)
            .translations.pt
        : countries.find((c) => c.alpha2Code === lastSubmit.country).name;
    if (
      birthDate.getMonth() === todaysDate.getMonth() &&
      birthDate.getDate() < todaysDate.getDate()
    ) {
      ++age;
    } else if (birthDate.getMonth() < todaysDate.getMonth()) {
      ++age;
    }
    return (
      <div>
        {t("Hello")} {lastSubmit.name + " " + lastSubmit.surname} {t("from")}{" "}
        {displayCountry}. {t("On")} {birthDate.getDate()} {t("of")}{" "}
        {t(monthNames[birthDate.getMonth()])} {t("you will be")}{" "}
        {(age === 0 ? 1 : age) +
          (age === 1 || age === 0 ? " " + t("year") : " " + t("years"))}{" "}
        {t("old")}!
      </div>
    );
  };

  return (
    <Form
      pathname={pathname}
      selectedUser={selectedUser}
      animation={animate}
      routes={routes}
      countries={countries}
      isLogged={isLogged}
      username={username}
      translate={t}
      calculateSubmitMessage={calculateSubmitMessage}
      logout={handleLogout}
      handleAnimationEnd={handleAnimationEnd}
      saveUser={handleSaveUser}
    />
  );
};

export default withNamespaces()(FormContainer);
