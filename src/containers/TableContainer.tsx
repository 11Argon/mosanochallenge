import React from "react";
import "../css/Style.css";
import Table from "../components/Table";
import { InitialStoreI } from "../objects";
import { useSelector, useDispatch } from "react-redux";
import { setSelectedUser, setAlertAnimation } from "../actions";
import { withNamespaces } from "react-i18next";

const TableContainer = ({ t }: any) => {
  const users = useSelector((store: InitialStoreI) => store.data.oldEntries);
  const countries = useSelector((store: InitialStoreI) => store.data.countries);
  const language = useSelector(
    (store: InitialStoreI) => store.application.language
  );

  const dispatch = useDispatch();

  const handleTableRowClick = (event: any, id?: number) => {
    const [...usersClone] = users || [];
    let clickedIndex = usersClone.findIndex((u) => u.id === id);
    if (clickedIndex > -1) {
      let user = usersClone[clickedIndex];
      if (!user.selected) {
        dispatch(setSelectedUser(user));
        dispatch(setAlertAnimation(true));
      }
    }
  };

  return (
    <Table
      users={users}
      tableRowClick={handleTableRowClick}
      translate={t}
      language={language}
      countries={countries}
    />
  );
};

export default withNamespaces()(TableContainer);
