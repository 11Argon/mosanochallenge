import React, { useState } from "react";
import "../css/Style.css";
import { LoginI } from "../objects";
import { useDispatch } from "react-redux";
import { logIn } from "../actions";
import { withNamespaces } from "react-i18next";
import Login from "../components/Login";
import Loading from "../components/Loading";

const LoginContainer = ({ t }: any) => {
  const [loading, setLoading] = useState<boolean>(false);
  const dispatch = useDispatch();

  const handleLogin = async (values: LoginI) => {
    setLoading(true);
    setTimeout(() => {
      dispatch(logIn(values));
    }, 2000);
  };
  return !!loading ? (
    <div className="col-md-12 table-wrap pt-2">
      <Loading />
    </div>
  ) : (
    <Login translate={t} handleLogin={handleLogin} />
  );
};

export default withNamespaces()(LoginContainer);
