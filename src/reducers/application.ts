const initialState = {
  language: localStorage.getItem("lang") ? localStorage.getItem("lang") : "pt",
};

type AppAction = {
  type: string;
  language: string;
};
const application = (state = initialState, action: AppAction) => {
  switch (action.type) {
    case "SET_LANGUAGE":
      return Object.assign({}, state, {
        language: action.language,
      });
    default:
      return state;
  }
};

export default application;
