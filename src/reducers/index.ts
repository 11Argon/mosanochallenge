import { combineReducers } from "redux";
import auth from "./auth";
import data from "./data";
import application from "./application";

export default combineReducers({
  auth,
  data,
  application,
});
