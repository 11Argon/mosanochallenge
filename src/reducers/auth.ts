const initialState = {
  isLogged: false,
  username: "",
};

type AuthAction = {
  type: string;
  username: string;
};
const auth = (state = initialState, action: AuthAction) => {
  switch (action.type) {
    case "LOG_IN":
      return Object.assign({}, state, {
        isLogged: true,
        username: action.username,
      });
    case "LOG_OUT":
      return Object.assign({}, state, initialState);
    default:
      return state;
  }
};

export default auth;
