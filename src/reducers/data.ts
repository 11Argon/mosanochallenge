import { UserI } from "../objects";

const initialState: IState = {
  countries: [],
  selectedUser: {},
  oldEntries: [],
  animate: false,
};
type IState = {
  countries: any[];
  selectedUser: any;
  oldEntries: UserI[];
  animate: boolean;
};
type AuthAction = {
  type: string;
  countries: any[];
  selectedUser: UserI;
  oldEntries: UserI[];
  animate: boolean;
};
const data = (state = initialState, action: AuthAction) => {
  switch (action.type) {
    case "SET_COUNTRIES":
      return Object.assign({}, state, {
        countries: action.countries,
      });
    case "SET_SELECTED_USER":
      let users = [...state.oldEntries];
      let selectedIndex = users.findIndex((u) => u.selected === true);
      if (selectedIndex > -1) {
        users[selectedIndex].selected = false;
      }
      action.selectedUser.selected = true;
      return Object.assign({}, state, {
        selectedUser: action.selectedUser,
        oldEntries: users,
      });
    case "SET_OLD_ENTRIES":
      return Object.assign({}, state, {
        oldEntries: action.oldEntries,
      });
    case "SET_ALERT_ANIMATION":
      return Object.assign({}, state, {
        animate: action.animate,
      });
    default:
      return state;
  }
};

export default data;
