import React from "react";
import { useSelector } from "react-redux";
import { InitialStoreI } from "../objects";
import Login from "../containers/LoginContainer";

type ExtraInfoType = {
  message: string;
};

function withAuthRequired<P>(WrappedComponent: React.FC<P>) {
  const ComponentWithExtraInfo = (props: P) => {
    const isLogged = useSelector((store: InitialStoreI) => store.auth.isLogged);

    return isLogged ? (
      <div>
        <WrappedComponent {...props} message={"asd"} />
      </div>
    ) : (
      <Login />
    );
  };
  return ComponentWithExtraInfo;
}

export default withAuthRequired;
