import React, { useState, useEffect } from "react";
import "./App.css";
import Navigation from "./components/Navigation";
import Axios from "axios";
import { useDispatch } from "react-redux";
import { setCountries } from "./actions";
import Loading from "./components/Loading";
import ErrorPage from "./components/ErrorPage";

function App() {
  const [loading, setLoading] = useState<boolean>(true);
  const [error, setError] = useState<boolean>(false);

  const dispatch = useDispatch();

  useEffect(() => {
    (async function getCountries() {
      let data;
      try {
        let res = await Axios.get("https://restcountries.eu/rest/v2/all");
        data = res.data;
      } catch (err) {
        setError(true);
      }
      dispatch(setCountries(data));
      setTimeout(() => {
        setLoading(false);
      }, 2000);
    })();
  }, [dispatch]);

  return !loading ? error ? <ErrorPage /> : <Navigation /> : <Loading />;
}

export default App;
