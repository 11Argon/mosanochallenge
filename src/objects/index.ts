export interface UserI {
  id?: number;
  name: string;
  surname: string;
  country: string;
  birthday?: string;
  selected?: boolean;
}

export interface InitialStoreI {
  data: {
    countries: any[];
    selectedUser: UserI;
    oldEntries: UserI[];
    animate: boolean;
  };
  auth: { isLogged: boolean; username: string };
  application: { language: string };
}

export interface LoginI {
  username: string;
  password: string;
}
