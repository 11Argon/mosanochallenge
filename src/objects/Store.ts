import {createStore} from 'redux';
import rootReducer from '../reducers/'

const store = createStore(rootReducer,  (window && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ && (window as any).__REDUX_DEVTOOLS_EXTENSION_COMPOSE__()));
export default store;