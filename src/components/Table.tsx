import React from "react";
import "../css/Table.css";
import { UserI } from "../objects";
import moment from "moment";
import withAuthRequired from "../hoc/withAuthRequired";

interface Props {
  tableRowClick: (event: any, id?: number) => void;
  translate: (a: string) => string;
  users: UserI[];
  language: string;
  countries: any[];
}

const Table: React.FC<Props> = ({
  users,
  tableRowClick,
  translate,
  language,
  countries,
}) => {
  return (
    <div className="table-wrap">
      <table className="table table-bordered table-hover w-100">
        <thead>
          <tr>
            <th scope="col">{translate("Name")}</th>
            <th scope="col">{translate("Country")}</th>
            <th scope="col">{translate("Birthday")}</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr
              key={index}
              onClick={(e) => tableRowClick(e, user.id)}
              className={user.selected ? "selected" : ""}
            >
              <td>
                {user.name} {user.surname}
              </td>
              <td>
                {language === "pt"
                  ? countries.find((c) => c.alpha2Code === user.country)
                      .translations.pt
                  : countries.find((c) => c.alpha2Code === user.country).name}
              </td>
              <td>{moment(user.birthday).format("DD/MM/YYYY")}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default withAuthRequired(Table);
