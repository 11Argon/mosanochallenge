import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import FormInput from "./FormInput";
import "../css/Form.css";
import { LoginI } from "../objects/";
import { withNamespaces } from "react-i18next";

interface Props {
  handleLogin: (values: LoginI) => void;
  translate: any;
}

const Login = ({ translate, handleLogin }: any) => {
  const initialValues: LoginI = {
    username: "",
    password: "",
  };

  const getRequiredErrorMessage = (Field: string) => {
    return (
      translate("Field") + " " + translate(Field) + " " + translate("Required")
    );
  };

  const FormSchema = Yup.object().shape({
    username: Yup.string().required(getRequiredErrorMessage("Username")),
    password: Yup.string().required(getRequiredErrorMessage("Password")),
  });

  return (
    <div className="col-md-12 table-wrap pt-2">
      <div>
        <h5>{translate("LOGIN_MESSAGE")}</h5>
      </div>
      <Formik
        initialValues={initialValues}
        onSubmit={(values, { setSubmitting }) => {
          setSubmitting(true);
          handleLogin(values);
        }}
        validationSchema={FormSchema}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
        }) => (
          <form onSubmit={handleSubmit} className="mt-4">
            <div className="container-fluid">
              <div className="row">
                <FormInput
                  error={errors.username}
                  touched={touched.username}
                  value={values.username}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  label={translate("Username") + ":"}
                  name={"username"}
                  placeholder={translate("Name") + " " + translate("here")}
                />
                <FormInput
                  error={errors.password}
                  touched={touched.password}
                  value={values.password}
                  handleChange={handleChange}
                  handleBlur={handleBlur}
                  label={translate("Password") + ":"}
                  name={"password"}
                  placeholder={translate("Password") + " " + translate("here")}
                  type={"password"}
                />
                <div id="errors" className={"col-12 text-danger"} role="alert">
                  {Object.entries(touched).map((item, index) => {
                    if ((errors as any)[item[0]]) {
                      return <div key={index}>{(errors as any)[item[0]]}</div>;
                    }
                    return "";
                  })}
                </div>
                <div className="form-group col-12">
                  <button
                    type="submit"
                    className="button float-right"
                    disabled={isSubmitting}
                  >
                    Login
                  </button>
                </div>
              </div>
            </div>
          </form>
        )}
      </Formik>
    </div>
  );
};

export default withNamespaces()(Login);
