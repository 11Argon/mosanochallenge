import React from "react";
import { BrowserRouter as Router, Switch } from "react-router-dom";
import Table from "../containers/TableContainer";
import Form from "../containers/FormContainer";
import RouteWithSubRoutes from "./RouteWithSubRoutes";
import Language from "./Language";

export default function Navigation() {
  return (
    <Router>
      <div id="app" className="container">
        <div className="row">
          <Language />
          <div className="col-md-12">
            <Switch>
              <RouteWithSubRoutes
                path={"/"}
                component={Form}
                routes={[
                  {
                    path: "/revisited",
                    component: Table,
                  },
                ]}
              />
            </Switch>
          </div>
        </div>
      </div>
    </Router>
  );
}
