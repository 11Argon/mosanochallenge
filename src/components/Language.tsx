import React from "react";
import i18next from "i18next";
import { useDispatch, useSelector } from "react-redux";
import { setLanguage } from "../actions";
import { InitialStoreI } from "../objects";
import "../css/Language.css";
const Language = () => {
  const dispatch = useDispatch();
  const currentLanguage = useSelector(
    (store: InitialStoreI) => store.application.language
  );
  const handleLanguageChange = (language: string): void => {
    if (currentLanguage !== language) {
      i18next.changeLanguage(language);
      dispatch(setLanguage(language));
      localStorage.setItem("lang", language);
    }
  };
  return (
    <div className="col-12">
      <div
        onClick={() => handleLanguageChange("pt")}
        style={{ width: "30px" }}
        className={
          currentLanguage === "pt" ? "selected float-left" : "float-left"
        }
      >
        <img src="/imgs/pt.png" alt="pt" className="w-100" />
      </div>
      <div
        onClick={() => handleLanguageChange("en")}
        style={{ width: "30px" }}
        className={
          currentLanguage === "en" ? "selected float-left" : "float-left"
        }
      >
        <img src="/imgs/en.png" alt="en" className="w-100" />
      </div>
    </div>
  );
};

export default Language;
