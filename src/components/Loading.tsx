import React from "react";
import "../css/Style.css";

const Loading = () => {
  return (
    <div className="loader-wrap">
      <div className="loader"></div>
    </div>
  );
};

export default Loading;
