import React from "react";
import { useSelector } from "react-redux";
import { InitialStoreI } from "../objects";
interface Props {
  error: string | undefined;
  touched: any;
  value: string | undefined;
  handleChange: any;
  handleBlur: any;
  label: string;
  options?: any[];
  name: string;
  placeholder?: string;
  type?: string;
  select?: boolean;
  min?: string;
  max?: string;
}

const FormInput: React.FC<Props> = (props) => {
  const {
    error,
    touched,
    value,
    handleChange,
    handleBlur,
    label,
    options,
    name,
    placeholder,
    ...rest
  } = props;
  const language = useSelector(
    (store: InitialStoreI) => store.application.language
  );
  return (
    <div className="form-group">
      <div className="row">
        <label htmlFor={name} className="col-3">
          {label}
        </label>
        <div className="col-9">
          {!props.select ? (
            <input
              name={name}
              type="text"
              className={
                error && touched ? "is-invalid form-control" : "form-control"
              }
              style={
                rest.type === "date" && value === ""
                  ? {
                      color: "rgb(133, 161, 187)",
                      backgroundPosition:
                        "right calc(0.375em + 0.1875rem + 35px) center",
                    }
                  : value !== ""
                  ? { border: "2px solid rgb(36, 122, 202)" }
                  : {}
              }
              autoComplete={name}
              value={value}
              onChange={handleChange}
              onBlur={handleBlur}
              placeholder={placeholder}
              {...rest}
            />
          ) : (
            options && (
              <select
                className={
                  error && touched ? "is-invalid form-control" : "form-control"
                }
                autoComplete={name}
                value={value}
                onChange={handleChange}
                onBlur={handleBlur}
                name={name}
                style={
                  value === ""
                    ? {
                        color: "rgb(133, 161, 187)",
                        backgroundPosition:
                          "right calc(0.375em + 0.1875rem + 15px) center",
                      }
                    : {
                        border: "2px solid rgb(36, 122, 202)",
                        backgroundPosition:
                          "right calc(0.375em + 0.1875rem + 15px) center",
                      }
                }
              >
                <option value={""} disabled={true}>
                  {placeholder}
                </option>
                {options.map((option, index) => (
                  <option key={option.alpha2Code} value={option.alpha2Code}>
                    {language === "pt" ? option.translations.pt : option.name}
                  </option>
                ))}
              </select>
            )
          )}
          <span className="focus-border">
            <i></i>
          </span>
        </div>
      </div>
    </div>
  );
};

export default FormInput;
