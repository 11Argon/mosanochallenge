import React from "react";
import { Formik } from "formik";
import * as Yup from "yup";
import FormInput from "./FormInput";
import { UserI } from "../objects";
import "../css/Form.css";
import { Link, Switch } from "react-router-dom";
import RouteWithSubRoutes from "./RouteWithSubRoutes";
import { isNumber } from "util";

interface Props {
  saveUser: (user: UserI) => void;
  handleAnimationEnd: (e: any) => void;
  translate: (a: string) => string;
  calculateSubmitMessage: (lastSubmit: UserI) => JSX.Element;
  logout: () => void;
  animation?: boolean;
  routes: any[];
  selectedUser: UserI;
  pathname: string;
  countries: any[];
  isLogged: boolean;
  username: string;
}

const Form: React.FC<Props> = ({
  saveUser,
  handleAnimationEnd,
  translate,
  calculateSubmitMessage,
  logout,
  animation,
  routes,
  selectedUser,
  pathname,
  countries,
  isLogged,
  username,
}) => {
  const initialValues: UserI = {
    name: "",
    surname: "",
    country: "",
    birthday: "",
  };

  const getRequiredErrorMessage = (Field: string) => {
    return (
      translate("Field") + " " + translate(Field) + " " + translate("Required")
    );
  };

  const FormSchema = Yup.object().shape({
    name: Yup.string().required(getRequiredErrorMessage("Name")),
    surname: Yup.string().required(getRequiredErrorMessage("Surname")),
    country: Yup.string().required(getRequiredErrorMessage("Country")),
    birthday: Yup.date().required(getRequiredErrorMessage("Birthday")),
  });

  const getCurrentDate = (): string => {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1;
    var yyyy = today.getFullYear();
    var ddString: string = dd < 10 ? "0" + dd : "" + dd;
    var mmString: String = mm < 10 ? "0" + mm : "" + mm;
    return yyyy + "-" + mmString + "-" + ddString;
  };

  return (
    <div className="row">
      <div className="col-12">
        {pathname === "/revisited" ? (
          <Link to="/" className="float-right btn btn-sm btn btn-dark mb-2">
            {translate("HIDE_MESSAGE")}
          </Link>
        ) : (
          <Link
            to="/revisited"
            className="float-right btn btn-sm btn btn-dark mb-2"
          >
            {translate("SHOW_MESSAGE")}
          </Link>
        )}
        {isLogged && (
          <button
            onClick={logout}
            className={"float-right btn btn-sm btn btn-danger mr-2"}
          >
            Logout
          </button>
        )}
      </div>
      <div className="col-md-6">
        <Formik
          initialValues={initialValues}
          onSubmit={(values, { setSubmitting }) => {
            setSubmitting(true);
            saveUser(values);
            setSubmitting(false);
          }}
          validationSchema={FormSchema}
        >
          {({
            values,
            errors,
            touched,
            handleChange,
            handleBlur,
            handleSubmit,
            isSubmitting,
          }) => (
            <form onSubmit={handleSubmit}>
              <div className="container-fluid">
                <div className="row">
                  <FormInput
                    error={errors.name}
                    touched={touched.name}
                    value={values.name}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    label={translate("Name") + ":"}
                    name={"name"}
                    placeholder={translate("Name") + " " + translate("here")}
                  />
                  <FormInput
                    error={errors.surname}
                    touched={touched.surname}
                    value={values.surname}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    label={translate("Surname") + ":"}
                    name={"surname"}
                    placeholder={translate("Surname") + " " + translate("here")}
                  />
                  <FormInput
                    error={errors.country}
                    touched={touched.country}
                    value={values.country}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    label={translate("Country") + ":"}
                    name={"country"}
                    type={"date"}
                    select
                    options={countries}
                    placeholder={translate("Countries")}
                  />
                  <FormInput
                    error={errors.birthday}
                    touched={touched.birthday}
                    value={values.birthday}
                    handleChange={handleChange}
                    handleBlur={handleBlur}
                    label={translate("Birthday") + ":"}
                    name={"birthday"}
                    type={"date"}
                    max={getCurrentDate()}
                  />
                  <div
                    id="errors"
                    className={"col-12 text-danger"}
                    role="alert"
                  >
                    {Object.entries(touched).map((item, index) => {
                      if ((errors as any)[item[0]]) {
                        return (
                          <div key={index}>{(errors as any)[item[0]]}</div>
                        );
                      }
                      return "";
                    })}
                  </div>
                  <div className="form-group col-12">
                    <button
                      type="submit"
                      className="button float-right"
                      disabled={isSubmitting}
                    >
                      {translate("Save")}
                    </button>
                  </div>
                  <div
                    className={
                      Object.entries(selectedUser).length === 0
                        ? "invisible col-12 alert alert-success"
                        : "col-12 alert alert-success"
                    }
                    role="alert"
                  >
                    <div
                      onAnimationEnd={handleAnimationEnd}
                      className={animation ? "alertAnimate" : ""}
                    >
                      {isNumber(selectedUser.id)
                        ? calculateSubmitMessage(selectedUser)
                        : ""}
                    </div>
                  </div>
                </div>
              </div>
            </form>
          )}
        </Formik>
      </div>
      <div className="col-md-6">
        <Switch>
          {routes.map((route, i) => (
            <RouteWithSubRoutes key={i} {...route} />
          ))}
        </Switch>
        {isLogged && username && (
          <h6 id="welcome-message" className="float-left">
            {translate("Welcome") + " " + username}!
          </h6>
        )}
      </div>
    </div>
  );
};

export default Form;
