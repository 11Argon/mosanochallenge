import React from "react";
import "../css/Style.css";
import { withNamespaces } from "react-i18next";

const ErrorPage: React.FC = ({ t }: any) => {
  return (
    <div id="app" className="container">
      <div className="d-flex align-items-center" style={{ height: "100%" }}>
        <div className="row w-100">
          <div className="col-8">
            <h1 className="col-12">{t("ERROR_MESSAGE")}</h1>
            <h3 className="col-12">{t("ERROR_MESSAGE_BODY")}</h3>
          </div>
          <div className="col-4">
            <img
              className="w-100"
              src="imgs/error-icon.png"
              alt="Error"
              style={{ width: "200px" }}
            />
          </div>
        </div>
      </div>
    </div>
  );
};

export default withNamespaces()(ErrorPage);
