import { UserI, LoginI } from "../objects";

export const logIn = ({ username }: LoginI) => ({
  type: "LOG_IN",
  username: username,
});

export const logOut = () => ({
  type: "LOG_OUT",
});

export const setCountries = (countries: any) => ({
  type: "SET_COUNTRIES",
  countries: countries,
});

export const setOldEntries = (users: UserI[]) => ({
  type: "SET_OLD_ENTRIES",
  oldEntries: users,
});

export const setSelectedUser = (user: UserI) => ({
  type: "SET_SELECTED_USER",
  selectedUser: user,
});

export const setAlertAnimation = (animate: boolean) => ({
  type: "SET_ALERT_ANIMATION",
  animate: animate,
});

export const setLanguage = (language: string) => ({
  type: "SET_LANGUAGE",
  language: language,
});
